const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence');
const { 
    MEMBER_GENDER,
    IS_SEND,
    MEMBER_TYPE,
    SUBSCRIBE_TYPE,
    STATUS
 } = require('../constants/variable');

const opts = { toJSON: { virtuals: true } };

const memberSchema = mongoose.Schema({
    memberSeq:  Number,
    nickname: { type: String, required: true, unique: true },
    gender: {
        type: String,
        enum: MEMBER_GENDER,
        required: true
    },
    phoneNumber: {
        type: Object,
        required: true,
        unique: true,
        sparse: true,
        properties: {
			dailCode: { type: String, required: true },
			phone: { type: String, required: true }
        },
        additionalProperties: false
    },
    avatar: String,
    introduce: String,
    notify: {
        type: String,
        enum: IS_SEND
    },
    type: {
        type: String,
        enum: MEMBER_TYPE,

    },
    channel: String,
    birthday: Date,
    status: {
        type: String,
        enum: STATUS
    },
    lastLoginTS: Date,
    parentID: String,
    lastVIPSubscribeHistory: {
        type: Object,
        // TODO: map this field with vip subscribe history table
        // type: [mongoose.Schema.Types.ObjectId],
        // ref: VipSubscribeHistory,
        properties: {
			type: {
                type: String,
                enum: SUBSCRIBE_TYPE
			},
			expiredTS: Date,
            historyID: { type: [mongoose.Schema.Types.ObjectId] }
        },
        additionalProperties: false
    },
    creationTS: { type: Date, default: Date.now() },
    modifyTS: Date,
    device: {
        type: Object,
        properties: {
            deviceType: String,
            deviceID: String
        },
        additionalProperties: false
    }
}, opts);

const settingDb = mongoose.connection.useDb('Setting');
memberSchema.plugin(AutoIncrement(settingDb), {id: 'member_seq', inc_field: 'memberSeq', disable_hooks: true});

const memberDb = mongoose.connection.useDb('Member');

/**
 * @typedef Member
 */
 const Member = memberDb.model('Members', memberSchema);

module.exports = Member;