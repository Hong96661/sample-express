const Joi = require('joi');
const { password } = require('./custom.validation');

const register = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    phone: Joi.object({
      countryCode: Joi.string().required(),
      number: Joi.string().required()
    }).required(),
  }),
};

const login = {
  body: Joi.object().keys({
    type: Joi.string().required(),
    phone: Joi.any().when('type', { 
      is: 'smsCode',
      then: Joi.object({
        countryCode: Joi.string().required(),
        number: Joi.string().required()
      }).required(),
      otherwise: Joi.optional() 
    }),
    
    password: Joi.any().when('type', { is: 'password', then: Joi.string().required(), otherwise: Joi.optional() }),

    email: Joi.any().when('type', { is: 'password', then: Joi.string().required(), otherwise: Joi.optional() }),
    // email: Joi.string().required(),
    // password: Joi.string().required(),
    
  }),
};

const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
  }),
};

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

const verifyEmail = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
};

const submitAuth = {
  body: Joi.object().keys({
    phoneNumber: Joi.object().keys({
      dialCode: Joi.string().required(),
      phone: Joi.string().required()
    }),
    nickname: Joi.string(),
    gender: Joi.string(),
    birthday: Joi.string(),
    isRegister: Joi.boolean().required()
  }),
};

module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  verifyEmail,
  submitAuth
};
