const STATUS = ["active", "inactive"];
const ENABLE = ["true", "false"];

const MEMBER_GENDER = ["boy", "girl"];
const IS_SEND = ["yes", "no"];
const MEMBER_TYPE = ["vip", "normal"];

const SUBSCRIBE_TYPE = ['monthly', 'quarterly', 'yearly'];

const PERMISSION = ['none', 'read', 'execute'];

const LANG_PROPERTY = { en: String, kh: String };

const lang = [
    { key: 'kh', value: 'Khmer' },
    { key: 'en', value: 'English' },
];

const EVENT_TYPE = ["drama", "camp"];
const CURRENCY = ["khr", "usd"];
const STATUS_ALL = ["cancel", "success", "closed", "pending"];

const YES_NO = ["yes", "no"];
const IS_GENDER = ["boy", "girl", "both"];
const IS_FREE = ["member", "visitor"];
const LABEL_TYPE = ["new", "recommend", "both"];
const STORY_TYPE = ["single episode", "multi-set story"];

module.exports = {
    STATUS,
    ENABLE,
    MEMBER_GENDER,
    IS_SEND,
    MEMBER_TYPE,
    SUBSCRIBE_TYPE,
    PERMISSION,
    LANG_PROPERTY,
    lang,
    EVENT_TYPE,
    CURRENCY,
    STATUS_ALL,
    YES_NO,
    IS_GENDER,
    IS_FREE,
    LABEL_TYPE,
    STORY_TYPE
}
