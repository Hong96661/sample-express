const httpStatus = require('http-status');
const { Member } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a member
 * @param {object} memberData
 * @returns {Promise<Member>}
 */
 const createMember = async (memberData) => {
    return Member.create(memberData);
};

/**
 * Get member by phone
 * @param {object} phoneNumber
 * @returns {Promise<Member>}
 */
const getMemberByPhone = async (phoneNumber) => {
    return Member.findOne({phoneNumber});
};

module.exports = {
    getMemberByPhone,
    createMember
  };
  