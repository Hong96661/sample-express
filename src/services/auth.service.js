const httpStatus = require('http-status');
const tokenService = require('./token.service');
const userService = require('./user.service');
const memberService = require('./member.service');
const Token = require('../models/token.model');
const ApiError = require('../utils/ApiError');
const { tokenTypes } = require('../config/tokens');

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  if (!user || !(await user.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }
  return user;
};

/**
 * Login with username and sms code
 * @param {string} phone
 * @param {string} smscode
 * @returns {Promise<User>}
 */
 const loginUserWithPhoneAndSMSCode = async (phone) => {
  const user = await userService.getUserByPhone(phone);

  if (!user) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect phone or password');
  }

  // const smsCode = await SMSCode.findOne({code: smscode});
  // if (!user || !smsCode) {
  //   throw new ApiError(httpStatus.UNAUTHORIZED, 'Verify Code is invalid!');
  // }else if(user || smsCode){
  //   // if check SMS code remove
  //   await smsCode.remove();
  // }
  return user;
};

/**
 * Logout
 * @param {string} refreshToken
 * @returns {Promise}
 */
const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({ token: refreshToken, type: tokenTypes.REFRESH, blacklisted: false });
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await refreshTokenDoc.remove();
};

/**
 * Refresh auth tokens
 * @param {string} refreshToken
 * @returns {Promise<Object>}
 */
const refreshAuth = async (refreshToken) => {
  try {
    const refreshTokenDoc = await tokenService.verifyToken(refreshToken, tokenTypes.REFRESH);
    const user = await userService.getUserById(refreshTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await refreshTokenDoc.remove();
    return tokenService.generateAuthTokens(user);
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
};

/**
 * Reset password
 * @param {string} resetPasswordToken
 * @param {string} newPassword
 * @returns {Promise}
 */
const resetPassword = async (resetPasswordToken, newPassword) => {
  try {
    const resetPasswordTokenDoc = await tokenService.verifyToken(resetPasswordToken, tokenTypes.RESET_PASSWORD);
    const user = await userService.getUserById(resetPasswordTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await userService.updateUserById(user.id, { password: newPassword });
    await Token.deleteMany({ user: user.id, type: tokenTypes.RESET_PASSWORD });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Password reset failed');
  }
};

/**
 * Verify email
 * @param {string} verifyEmailToken
 * @returns {Promise}
 */
const verifyEmail = async (verifyEmailToken) => {
  try {
    const verifyEmailTokenDoc = await tokenService.verifyToken(verifyEmailToken, tokenTypes.VERIFY_EMAIL);
    const user = await userService.getUserById(verifyEmailTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await Token.deleteMany({ user: user.id, type: tokenTypes.VERIFY_EMAIL });
    await userService.updateUserById(user.id, { isEmailVerified: true });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Email verification failed');
  }
};

/**
 * Submit auth if create new if not exist
 * @param {object} body
 * @returns {Promise}
 */
 const submitAuth = async (body) => {
   const {phoneNumber, isRegister} = body

  try {
    let member = await memberService.getMemberByPhone(phoneNumber);
    
    if (!member && isRegister) {
      member = await memberService.createMember(body)
    }

    return member;
  } catch (error) {
    throw error
  }
};

module.exports = {
  loginUserWithEmailAndPassword,
  loginUserWithPhoneAndSMSCode,
  logout,
  refreshAuth,
  resetPassword,
  verifyEmail,
  submitAuth
};
